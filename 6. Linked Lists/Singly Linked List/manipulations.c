#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;   
    struct node *next;
};



typedef struct node node;



node * creat_ll (node *);
void display_ll (node *);
node * insert_begin (node *);
node * insert_end (node *);
node * insert_before (node *);
node * insert_after (node *);
node * delete_begin (node *);
node * delete_end (node *);

/* 
 * This function isn't correct
 * I will fix it later
 */
node * delete_before (node *);
node * delete_after (node *);




int main (void)
{
    node *start = NULL;

    /* Test create linked list */
    start = creat_ll(start);
    display_ll(start);
    /* Test module insert */
    {
        /* Test insert at begining linked list */
        #ifdef begin
        start = insert_begin(start);
        display_ll(start);
        #endif

        /* Test insert at ending linked list */
        #ifdef end
        start = insert_end(start);
        display_ll(start);
        #endif

        /* Test insert before paticular position in linked list */
        #ifdef before
        start = insert_before(start);
        display_ll(start);
        #endif
        
        /* Test insert before paticular position in linked list */
        #ifdef after
        start = insert_after(start);
        display_ll(start);
        #endif
    }

    /* Test module delete */
    {
        /* Test delete begin */
        #ifdef d_begin
        start = delete_begin(start);
        display_ll(start);
        #endif

        /* Test delete end */
        #ifdef d_end
        start = delete_end(start);
        display_ll(start);
        #endif

        /* Test delete before particular position */
        #ifdef d_before
        start = delete_before(start);
        display_ll(start);
        #endif
        
        /* Test delete after particular position */
        #define d_after
        #ifdef d_after
        start = delete_after(start);
        display_ll(start);
        #endif

    }
    return 0;
}


node * delete_after (node *start)
{
    int value;
    puts("Delete element of linked list after special value");
    printf("Enter value: ");
    fflush(stdin);
    scanf("%d", &value);

    if ((start == NULL) || (start->next == NULL))
    {
        return NULL;
    }

    for (node *count = start; count->next != NULL; count = count->next)
    {
        if (count->data == value)
        {
            node *temp = count->next;
            count->next = temp->next;
            free(temp);
        }
    }

    return start;
}



node * delete_before (node *start)
{
    int value;
    puts("Delete element of linked list before special value");
    printf("Enter value: ");
    fflush(stdin);
    scanf("%d", &value);

    if ((start == NULL) || (start->next == NULL))
    {
        return NULL;
    }

    node *count;
    node *pre_count;
    for (count = start->next, pre_count = start; count != NULL; count = count->next)
    {
        if (count->data == value)
        {
            if (pre_count == start)
            {
                node *temp = pre_count;
                pre_count  = count;
                start      = pre_count;
                free(temp);
            }
            else
            {
                node *temp = count;
                pre_count->data = count->data;
                pre_count->next = count->next;
                count = count->next;
                free(temp);
            }
        }
        else
        {
            pre_count = count;
        }
    }
    

    return start;
}


node * delete_end (node *start)
{
    puts("Delete the last element of linked list");
    for (node *count = start; ; count = count->next)
    {
        if (count->next->next == NULL)
        {
            free(count->next);
            count->next = NULL;
            return start;   
        }
    }
}


node * delete_begin (node *start)
{
    puts("Delete the first element of linked list");
    node *temp = start;
    start = start->next;
    free(temp);

    return start;
} 


node * creat_ll (node *start)
{
    node *new_node;
    node *ptr;
    int data;
    puts("Enter data (q to quit)");
    while (scanf("%d", &data))
    {
        new_node = (node *) malloc(sizeof(node));
        new_node->data = data;
        if (start == NULL)
        {
            start = new_node;
            new_node->next = NULL;
        }
        else
        {
            ptr = start;
            while (ptr->next != NULL)
            {
                ptr = ptr->next;
            }
            ptr->next = new_node;
            new_node->next = NULL;
        }
        puts("Enter data (q to quit)");
    }

    return start;
}


void display_ll (node *start)
{
    node *count;
    for (count = start; count->next != NULL; count = count->next)
    {
        printf("%d->", count->data);
    }
    printf("%d\n", count->data);

    return ;
}


node * insert_begin (node *start)
{
    node *add = (node *) malloc(sizeof(node));
    puts("Insert at begining");
    puts("Enter data");
    fflush(stdin);
    scanf("%d", &add->data);
    add->next = start;
    start = add;

    return start;
}


node * insert_end (node *start)
{
    node *add = (node *) malloc(sizeof(node));
    node *count = start;
    puts("Insert at ending");
    puts("Enter data");
    fflush(stdin);
    scanf("%d", &add->data);
    for (count = start; count->next != NULL; count = count->next)
    {
        
    }
    count->next = add;
    add->next = NULL;

    return start;
}


node * insert_before (node *start)
{
    int value, data;
    puts("Insert before particular position in linked list");
    puts("Enter data");
    fflush(stdin);
    scanf("%d", &data);
    puts("Enter value which you want to insert before");
    fflush(stdin);
    scanf("%d", &value);
    if (start == NULL)
        return NULL;


    for (node *count = start, *pre_count = count; count != NULL; count = count->next)
    {
        if (count->data == value)
        {
            node *add = (node *) malloc(sizeof(node));
            add->data = data;

            if (count == start)
            {
                pre_count = add;
                add->next = count;
                start = pre_count;
            }
            else
            {
                pre_count->next = add;
                add->next = count;
            }
        }
        pre_count = count;
    }

    return start;
}


node * insert_after (node *start)
{
    int value, data;
    puts("Insert after particular position in linked list");
    puts("Enter data");
    fflush(stdin);
    scanf("%d", &data);
    puts("Enter value which you want to insert after");
    fflush(stdin);
    scanf("%d", &value);
    if (start == NULL)
        return NULL;
    
    for (node *count = start; count != NULL; count = count->next)
    {
        if (count->data == value)
        {
            node *add = (node *) malloc(sizeof(node));
            add->data = data;
            node *temp = count->next;
            count->next = add;
            add->next = temp;
        }
    }

    
    return start;
}