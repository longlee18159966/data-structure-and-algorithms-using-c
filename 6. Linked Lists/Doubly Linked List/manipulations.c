#include <stdio.h>
#include <stdlib.h>

struct node
{
    struct node *previous;
    int data;   
    struct node *next;
};


typedef struct node node;


node * creat_ll (node *);
void display_ll (node *);
void display_reverse_ll (node *);
node * insert_begin (node *);
node * insert_end (node *);
node * insert_before (node *);
node * insert_after (node *);
node * delete_begin (node *);
node * delete_end (node *);
node * delete_before (node *);
node * delete_after (node *);


int main (void)
{
    node *start = NULL;

    /* Test create linked list */
    start = creat_ll(start);
    display_ll(start);
    display_reverse_ll(start);
    /* Test module insert */
    {
        /* Test insert at begining linked list */
        #ifdef begin
        start = insert_begin(start);
        display_ll(start);
        display_reverse_ll(start);
        #endif

        /* Test insert at ending linked list */
        #ifdef end
        start = insert_end(start);
        display_ll(start);
        display_reverse_ll(start);
        #endif

        /* Test insert before paticular position in linked list */
        #ifdef before
        start = insert_before(start);
        display_ll(start);
        display_reverse_ll(start);
        #endif
        
        /* Test insert before paticular position in linked list */
        #ifdef after
        start = insert_after(start);
        display_ll(start);
        display_reverse_ll(start);
        #endif
    }

    /* Test module delete */
    {
        /* Test delete begin */
        #ifdef d_begin
        start = delete_begin(start);
        display_ll(start);
        display_reverse_ll(start);
        #endif

        /* Test delete end */
        #ifdef d_end
        start = delete_end(start);
        display_ll(start);
        display_reverse_ll(start);
        #endif

        /* Test delete before particular position */
        #ifdef d_before
        start = delete_before(start);
        display_ll(start);
        display_reverse_ll(start);
        #endif
        
        /* Test delete after particular position */
        #ifdef d_after
        start = delete_after(start);
        display_ll(start);
        display_reverse_ll(start);
        #endif

    }

    return 0;
}


/*
 * Delete node after particular position in linked list
 */
node * delete_after (node *start)
{
    puts("Delte node after particular position in linked list");
    puts("Enter value at position, which you want to delete after it");
    int value;
    fflush(stdin);
    scanf("%d", &value);
    node *count = NULL;
    for (count = start; count->next != NULL; count = count->next)
    {
        if ((count->next->next == NULL) && (count->data == value))
        {
            node *temp = count->next;
            count->next = temp->next;
            free(temp);   
            break;
        }
        else if (count->data == value)
        {
            node *temp = count->next;
            count->next = temp->next;
            temp->next->previous = count;
            free(temp);
        }
    }
   
    return start;
}


/*
 * Delete node before particular position in linked list
 */
node * delete_before (node *start)
{
    puts("Delte node before particular position in linked list");
    puts("Enter value at position, which you want to delete before it");
    int value;
    fflush(stdin);
    scanf("%d", &value);
    if ((start == NULL) || (start->next == NULL))
        return start;

    node *count = NULL;
    for (count = start->next; count != NULL; count = count->next)
    {
        if ((count == start->next) && (count->data == value))
        {
            node *temp = start;
            count->previous = NULL;
            start = count;
            free(temp);
        }
        else if (count->data == value)
        {
            node *temp = count->previous;
            count->previous = temp->previous;
            temp->previous->next = count;
            free(temp);
        }
    }
    
    return start;
}


/*
 * Delete node at the end of the linked list
 */
node * delete_end (node *start)
{
    puts("Delete at the end of the linked list");
    node *count;
    for (count = start; count->next != NULL; count = count->next)
    {

    }
    count->previous->next = NULL;
    free(count);

    return start;
}


/*
 * Delete node at beginning of the linked list
 */
node * delete_begin (node *start)
{
    puts("Delete node at the beginning of the linked list");
    node *temp = start;
    start = start->next;
    start->previous = NULL;
    free(temp);

    return start;
}


/* 
 * Insert node after special position in linked list
 */
node * insert_after (node *start)
{
    puts("Insert node after particular position in linked list");
    puts("Enter the value of the position, which you want insert to");
    int value = 0;
    fflush(stdin);
    scanf("%d", &value);
    puts("Enter the new data");
    int data = 0;
    fflush(stdin);
    scanf("%d", &data);
    node *count = NULL;
    for (count = start; count != NULL; count = count->next)
    {
        if ((count->next == NULL) && (count->data == value))
        {
            node *add = (node *) malloc(sizeof(node));
            add->data = data;
            add->next = count->next;
            count->next = add;
            add->previous = count;
        }
        else if (count->data == value)
        {
            node *add = (node *) malloc(sizeof(node));
            add->data = data;
            add->next = count->next;
            count->next->previous = add;
            count->next = add;
            add->previous = count;
        }
    }
    
    return start;
}


/*
 * Insert node before special position in linked list
 */
node * insert_before (node *start)
{
    puts("Insert node before particular position in linked list");
    puts("Enter the value of the position, which you want insert to");
    int value = 0;
    fflush(stdin);
    scanf("%d", &value);
    puts("Enter the new data");
    int data = 0;
    fflush(stdin);
    scanf("%d", &data);
    node *count = NULL;
    for (count = start; count != NULL; count = count->next)
    {
        if ((count == start) && (count->data == value))
        {
            node *add = (node *) malloc(sizeof(node));
            add->data = data;
            add->previous = count->previous;
            add->next = count;
            count->previous = add;
            start = add;
               
        }
        else if (count->data == value)
        {
            node *add = (node *) malloc(sizeof(node));
            add->data = data;
            add->previous = count->previous;
            count->previous->next = add;
            add->next = count;
            count->previous = add;
        }
    }
        
    return start;
}


/* 
 * Insert node at the end of linked list
 */
node * insert_end (node *start)
{
    node *count = NULL;
    int data = 0;
    puts("Insert node at the end of linked list");
    puts("Enter data for new node");
    fflush(stdin);
    scanf("%d", &data);
    node *add = (node *) malloc(sizeof(node));
    add->data = data;
    for (count = start; count->next != NULL; count = count->next)
    {

    }
    add->previous = count;
    count->next = add;
    add->next = NULL;
    
    return start;
}


/* 
 * Insert node at the beginning of linked list 
 */
node * insert_begin (node *start)
{
    int data = 0;
    puts("Insert node at the beginning of linked list");
    puts("Enter data for new node");
    fflush(stdin);
    scanf("%d", &data);
    node *add = (node *) malloc(sizeof(node));
    start->previous = add;
    add->data = data;
    add->previous = NULL;
    add->next = start;
    start = add;

    return start;
}


/*
 * Display linked list reverse
 */
void display_reverse_ll (node *start)
{
    puts("Display reverse");
    node *count_ll;
    for (count_ll = start; count_ll->next != NULL; count_ll = count_ll->next)
    {
    }
    while (count_ll->previous != NULL)
    {
        printf("%d->", count_ll->data);
        count_ll = count_ll->previous;
    }
    printf("%d\n", count_ll->data);
}


/*
 * Display linked list
 */
void display_ll (node *start)
{
    puts("Display");
    node *count_ll;
    for (count_ll = start; count_ll->next != NULL; count_ll = count_ll->next)
    {
        printf("%d->", count_ll->data);
    }
    printf("%d\n", count_ll->data);
}


/* 
 * Create linked list
 */
node * creat_ll (node *start)
{
    node *new_node;
    node *ptr;
    int data;
    puts("Enter data (q to quit)");
    while (scanf("%d", &data))
    {
        new_node = (node *) malloc(sizeof(node));
        new_node->data = data;
        if (start == NULL)
        { 
            start = new_node;
            new_node->next = NULL;
            new_node->previous = NULL;
        }
        else
        {
            ptr = start;
            while (ptr->next != NULL)
            {
                ptr = ptr->next;
            }
            ptr->next = new_node;
            new_node->next = NULL;
            new_node->previous = ptr;
        }
        puts("Enter data (q to quit)");
    }

    return start;
}