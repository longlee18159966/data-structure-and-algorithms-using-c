
#ifndef __ARR_H
#define __ARR_H

/* ********************************************* INCLUDE **************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/* ********************************************* DEFINE **************************************** */

#define DATA_TYPE int




/* ********************************************* FUNCTION PROTOTYPE **************************************** */

/* 
 * @brief: display element of array
 */
void arr_display (DATA_TYPE *A, int length);


/* 
 * @brief: insert element in middle array
 */
bool arr_insert (DATA_TYPE *A, int length, int pos, DATA_TYPE val);


/* 
 * @brief: delete element in middle array
 */
int arr_delete (DATA_TYPE *A, int length, int pos);

#endif