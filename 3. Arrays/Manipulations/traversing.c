#include "arrays.h"


void arr_display (DATA_TYPE *A, int length)
{
    int i;
    for (i = 0; i < length; ++i)
    {
        printf("A[%d] = %d\n", i, A[i]);
    }
}