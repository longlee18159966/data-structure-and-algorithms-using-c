#include "arrays.h"

int main (void)
{
    DATA_TYPE integer[10] = {2, 4, 5, 6, 1, 16, 81};
    arr_display(integer, sizeof(integer) / sizeof(DATA_TYPE));
    arr_insert(integer, 10, 2, 222);
    arr_insert(integer, 10, 9, 999);
    puts("\n ************************************** \n");
    arr_display(integer, sizeof(integer) / sizeof(DATA_TYPE));
    puts("\n ************************************** \n");
    int length = arr_delete(integer, 10, 4);
    arr_display(integer, length);

    return 0;
}