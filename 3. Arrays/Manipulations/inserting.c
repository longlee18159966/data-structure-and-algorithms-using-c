
#include "arrays.h"


bool arr_insert (DATA_TYPE *A, int length, int pos, DATA_TYPE val)
{
    if (pos >= length)
    {
        return false;
    }
    int i;
    for (i = length - 2; i >= pos; --i)
    {
        A[i + 1] = A[i];
    }
    A[pos] = val;
    
    return true;
}