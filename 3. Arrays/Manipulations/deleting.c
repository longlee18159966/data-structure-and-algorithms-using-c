
#include "arrays.h"


int arr_delete (DATA_TYPE *A, int length, int pos)
{
    if (pos >= length)
        return 0;

    int i;
    for (i = pos; i < length; ++i)
    {
        A[i] = A[i+1];
    }

    return length - 1;
}